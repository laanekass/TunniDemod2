package ee.bcs.koolitus.mammal;

import java.util.Objects;

public class Human extends Mammal implements MammalInt, Comparable<Human> {
	public boolean isStanding = false;

	private String address;
	private String birthday;

	@Override
	public void moveAhead() {
		if (!isStanding) {
			standUp();
		}
		makeStepWithLeg(Leg.LEFT);
		makeStepWithLeg(Leg.RIGHT);
	}

	private void standUp() {
		System.out.println(this.speciesName + ", human, stands up");
	}

	private void makeStepWithLeg(Leg leg) {
		System.out.println(this.speciesName + ", human, made a step ahead with " + leg + " leg.");
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Human other = (Human) obj;
		boolean variablesEqual = this.speciesName.equals(other.speciesName) && this.gender == other.gender
				&& this.address.equals(other.address) && this.birthday.equals(other.birthday);
		return variablesEqual;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 39 * hash + Objects.hashCode(speciesName);
		hash = 39 * hash + Objects.hashCode(gender);
		hash = 39 * hash + Objects.hashCode(address);
		hash = 39 * hash + Objects.hashCode(birthday);
		return hash;
	}

	@Override
	public int compareTo(Human human) {
		if (this.equals(human)) {
			return 0;
		} else if (this.speciesName.compareTo(human.speciesName) != 0) {
			return this.speciesName.compareTo(human.speciesName);
		} else if (compareBirthDates(human) != 0) {
			return compareBirthDates(human);
		}
		return 0;
	}

	private int compareBirthDates(Human human) {
		if (this.birthday.compareTo(human.birthday) == 0) {
			return 0;
		} else if (this.birthday.split("\\.")[2].compareTo(human.birthday.split("\\.")[2]) < 0) {
			return -1;
		} else if (this.birthday.split("\\.")[2].compareTo(human.birthday.split("\\.")[2]) == 0
				&& this.birthday.split("\\.")[1].compareTo(human.birthday.split("\\.")[1]) < 0) {
			return -1;
		} else if (this.birthday.split("\\.")[2].compareTo(human.birthday.split("\\.")[2]) == 0
				&& this.birthday.split("\\.")[1].compareTo(human.birthday.split("\\.")[1]) == 0
				&& this.birthday.split("\\.")[0].compareTo(human.birthday.split("\\.")[0]) < 0) {
			return -1;
		} else {
			return 1;
		}
	}

	@Override
	public String toString() {
		return "Human [address=" + address + ", birthday=" + birthday + ", speciesName=" + speciesName + ", gender="
				+ gender + "]";
	}

	

}
