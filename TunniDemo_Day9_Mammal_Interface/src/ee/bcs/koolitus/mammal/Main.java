package ee.bcs.koolitus.mammal;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class Main {

	public static void main(String[] args) {
		Human human = new Human();
		human.defineSpeciesName("Mart");
		human.moveAhead();

		Mammal dolphin = new Dolphin();
		dolphin.defineSpeciesName("Lucky");
		dolphin.moveAhead();

		Human humanEq1 = new Human();
		humanEq1.setAddress("Kurrunurru");
		humanEq1.setBirthday("25.04.1999");
		humanEq1.setGender(Gender.FEMALE);
		humanEq1.defineSpeciesName("Pipi");

		Human humanEq2 = new Human();
		humanEq2.setAddress("Kurrunurru");
		humanEq2.setBirthday("24.04.1999");
		humanEq2.setGender(Gender.FEMALE);
		humanEq2.defineSpeciesName("Pipi");

		System.out.println("humanEq1 == humanEq2? :" + humanEq1.equals(humanEq2));

		System.out.println("humanEq1 räsi == humanEq2 räsi? :" + (humanEq1.hashCode() == humanEq2.hashCode()));
		
		Set<Human> humans = new TreeSet<>(Collections.reverseOrder());
		humans.add(humanEq1);
		humans.add(humanEq2);
		System.out.println(humans.size());
		
		for(Human h: humans) {
			System.out.println(h);
		}
	}
}
