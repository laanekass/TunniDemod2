package ee.bcs.koolitus.mammal;

public abstract class Mammal implements MammalInt {
	String speciesName;
	Gender gender;
	
	public Mammal defineSpeciesName(String speciesName) {
		this.speciesName = speciesName;
		return this;
	}
	
	public Mammal setGender(Gender gender) {
		this.gender = gender;
		return this;
	}

}
