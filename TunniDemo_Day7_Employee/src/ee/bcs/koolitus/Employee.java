package ee.bcs.koolitus;

import java.math.BigDecimal;

public class Employee {
	private BigDecimal salary = BigDecimal.ZERO;

	public BigDecimal getSalary() {
		return salary;
	}

	public Employee setSalary(BigDecimal salary) {
		this.salary = salary;
		return this;
	}
}
