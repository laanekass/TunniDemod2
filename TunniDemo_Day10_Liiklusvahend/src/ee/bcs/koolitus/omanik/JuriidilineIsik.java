package ee.bcs.koolitus.omanik;

import java.util.ArrayList;
import java.util.List;

public class JuriidilineIsik implements Omanik {
	private static int counter = 0;
	private int id;
	private String address;
	private String registrationNumber;
	private String jurIsikuNimi;
	private List<Inimene> owner = new ArrayList<>();

	public JuriidilineIsik() {
		counter++;
		id = counter;
	}

	public int getId() {
		return id;
	}

	@Override
	public String getAddress() {
		return address;
	}

	@Override
	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String getOmanikuNimi() {
		return this.jurIsikuNimi;
	}

	public void setJurIsikuNimi(String jurIsikuNimi) {
		this.jurIsikuNimi = jurIsikuNimi;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public List<Inimene> getOwner() {
		return owner;
	}

	public void setOwner(List<Inimene> owner) {
		this.owner = owner;
	}

	@Override
	public String toString() {
		return "JuriidilineIsik [id=" + id + ", address=" + address + ", registrationNumber=" + registrationNumber
				+ ", owner=" + owner + "]";
	}

}
