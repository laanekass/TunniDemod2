package ee.bcs.koolitus.omanik;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ee.bcs.koolitus.liiklusvahend.JuhiloaKategooria;

public class Inimene {
	private static int counter = 0;
	private int id;
	private String eesnimi;
	private String perenimi;
	private Date synnikuupaev;
	private List<JuhiloaKategooria> juhiloaKategooriad = new ArrayList<>();

	public Inimene() {
		counter++;
		id = counter;
	}

	public int getId() {
		return id;
	}

	public String getEesnimi() {
		return eesnimi;
	}

	public Inimene setEesnimi(String eesnimi) {
		this.eesnimi = eesnimi;
		return this;
	}

	public String getPerenimi() {
		return perenimi;
	}

	public Inimene setPerenimi(String perenimi) {
		this.perenimi = perenimi;
		return this;
	}

	public Date getSynnikuupaev() {
		return synnikuupaev;
	}

	public Inimene setSynnikuupaev(Date synnikuupaev) {
		this.synnikuupaev = synnikuupaev;
		return this;
	}

	public List<JuhiloaKategooria> getJuhiloaKategooriad() {
		return juhiloaKategooriad;
	}

	public Inimene setJuhiloaKategooriad(List<JuhiloaKategooria> juhiloaKategooriad) {
		this.juhiloaKategooriad = juhiloaKategooriad;
		return this;
	}

	public List<JuhiloaKategooria> lisaJuhiloaKategooria(JuhiloaKategooria kategooria) {
		if (!this.juhiloaKategooriad.contains(kategooria)) {
			this.juhiloaKategooriad.add(kategooria);
		}
		return this.juhiloaKategooriad;
	}

	@Override
	public String toString() {
		return "Inimene [eesnimi=" + eesnimi + ", perenimi=" + perenimi + ", synnikuupaev=" + synnikuupaev
				+ ", juhiloaKategooriad=" + juhiloaKategooriad + "]";
	}
}
