package ee.bcs.koolitus.omanik;

public class Eraisik extends Inimene implements Omanik{
	private String address;

	@Override
	public String getAddress() {
		return address;
	}

	@Override
	public void setAddress(String address) {
		this.address = address;		
	}
	
	@Override
	public String getOmanikuNimi(){
		return this.getEesnimi() + " " + this.getPerenimi();
	}

	@Override
	public String toString() {
		return "Eraisik [address=" + address + ", inimene=" + super.toString() + "]";
	}

}
