package ee.bcs.koolitus.omanik;

public interface Omanik {
	int getId();
	String getAddress();
	void setAddress(String address);
	String getOmanikuNimi();
}
