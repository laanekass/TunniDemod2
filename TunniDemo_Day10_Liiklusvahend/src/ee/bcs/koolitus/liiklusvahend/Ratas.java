package ee.bcs.koolitus.liiklusvahend;

import ee.bcs.koolitus.omanik.Omanik;

public class Ratas extends LiikumisVahendImpl implements Comparable<Ratas> {
	private int kaikudeArv;
	private IstumisAsend istumisAsend;
	private String seeriaNumber;

	public Ratas(Omanik omanik) {
		super(omanik);
	}

	@Override
	public void soidab(String algus, String lopp) {
		// TODO Auto-generated method stub

	}

	public int getKaikudeArv() {
		return kaikudeArv;
	}

	public Ratas setKaikudeArv(int kaikudeArv) {
		this.kaikudeArv = kaikudeArv;
		return this;
	}

	public IstumisAsend getIstumisAsend() {
		return istumisAsend;
	}

	public Ratas setIstumisAsend(IstumisAsend istumisAsend) {
		this.istumisAsend = istumisAsend;
		return this;
	}

	public String getSeeriaNumber() {
		return seeriaNumber;
	}

	public Ratas setSeeriaNumber(String seeriaNumber) {
		this.seeriaNumber = seeriaNumber;
		return this;
	}

	@Override
	public String toString() {
		return "Ratas [kaikudeArv=" + kaikudeArv + ", istumisAsend=" + istumisAsend + ", seeriaNumber=" + seeriaNumber
				+ ", id=" + getId() + ", omanikud=" + getOmanikud() + "]";
	}

	@Override
	public int compareTo(Ratas otherRatas) {
		if (this.equals(otherRatas)) {
			return 0;
		}
		if (this.seeriaNumber.compareTo(otherRatas.seeriaNumber) != 0) {
			return this.seeriaNumber.compareTo(otherRatas.seeriaNumber);
		} else if (this.istumisAsend.compareTo(otherRatas.istumisAsend) != 0) {
			this.istumisAsend.compareTo(otherRatas.istumisAsend);
		} else if (this.kaikudeArv < otherRatas.kaikudeArv) {
			return -1;
		} else if (this.kaikudeArv > otherRatas.kaikudeArv) {
			return 1;
		}
		return 0;
	}

}
