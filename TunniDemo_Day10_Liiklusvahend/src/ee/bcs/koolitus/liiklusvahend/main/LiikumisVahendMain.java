package ee.bcs.koolitus.liiklusvahend.main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import ee.bcs.koolitus.liiklusvahend.IstumisAsend;
import ee.bcs.koolitus.liiklusvahend.JuhiloaKategooria;
import ee.bcs.koolitus.liiklusvahend.LiikumisVahendImpl;
import ee.bcs.koolitus.liiklusvahend.Ratas;
import ee.bcs.koolitus.liiklusvahend.Rula;
import ee.bcs.koolitus.liiklusvahend.mootorsoiduk.Auto;
import ee.bcs.koolitus.liiklusvahend.mootorsoiduk.Mootorsoiduk;
import ee.bcs.koolitus.liiklusvahend.mootorsoiduk.Veesoiduk;
import ee.bcs.koolitus.omanik.Eraisik;
import ee.bcs.koolitus.omanik.JuriidilineIsik;
import ee.bcs.koolitus.omanik.Omanik;

public class LiikumisVahendMain {

	public static void main(String[] args) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

		Eraisik inimene = new Eraisik();
		inimene.setEesnimi("Kati").setPerenimi("Kask")
				.setSynnikuupaev(formatter.parse("25.06.1991"));
		inimene.setAddress("Kuuse talu");
		inimene.lisaJuhiloaKategooria(JuhiloaKategooria.A1);
		inimene.lisaJuhiloaKategooria(JuhiloaKategooria.B1);
		System.out.println(inimene);
		
		JuriidilineIsik jurIsik = new JuriidilineIsik();
		jurIsik.setJurIsikuNimi("Kase OÜ");
		jurIsik.setAddress("Aia 6, Tallinn");
		jurIsik.setOwner(Arrays.asList(inimene));

		Set<LiikumisVahendImpl> soidukid = new HashSet<>();
		soidukid.add(new Ratas(inimene).setSeeriaNumber("982345AB").setIstumisAsend(IstumisAsend.PYSTINE));
		soidukid.add(new Ratas(inimene).setSeeriaNumber("817mng").setIstumisAsend(IstumisAsend.SELILI));
		soidukid.add(new Rula(inimene));
		soidukid.add(new Auto(jurIsik, "123ABC", JuhiloaKategooria.B1));
		soidukid.add(new Auto(inimene, "321ABC", JuhiloaKategooria.C));
		soidukid.add(new Veesoiduk(jurIsik, "12mnb", JuhiloaKategooria.VAIKE_PAAT));

		for (LiikumisVahendImpl liikumisVahend : soidukid) {
			System.out.println(liikumisVahend);
		}

		Set<Ratas> rattad = new TreeSet<>();
		for (LiikumisVahendImpl lv : soidukid) {
			if (lv instanceof Ratas) {
				rattad.add((Ratas) lv);
			}
		}

		System.out.println(rattad);

		for (LiikumisVahendImpl lv : soidukid) {
			System.out.println("Omanik: " + lv.getOmanikud());
		}
		
		System.out.println("Eraisikud:");
		for (LiikumisVahendImpl lv : soidukid) {
			for (Omanik omanik: lv.getOmanikud().values())
			{
				if(omanik.getClass().equals(Eraisik.class)){
					System.out.println("Omanik: " + lv.getOmanikud());
				}
			}
			
		}
		System.out.println("Juriidilised isikud:");
		for (LiikumisVahendImpl lv : soidukid) {
			for (Omanik omanik: lv.getOmanikud().values())
			{
				if(omanik.getClass().equals(JuriidilineIsik.class)){
					System.out.println("Omanik: " + lv.getOmanikud());
				}
			}			
		}
		
		System.out.println(Mootorsoiduk.getMootorSoidukOmanikuNimiJargi("Kati Kask"));
		System.out.println(Mootorsoiduk.getMootorSoidukOmanikuNimiJargi("Kase OÜ"));
		System.out.println(Mootorsoiduk.getMootorSoidukRegNrJargi("321ABC"));
	}
}
