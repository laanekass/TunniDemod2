package ee.bcs.koolitus.liiklusvahend;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import ee.bcs.koolitus.omanik.Omanik;

public abstract class LiikumisVahendImpl implements LiikumisVahendInt {
	private static int counter = 0;
	private int id;
	private Map<Integer, Omanik> omanikud = new HashMap<>();

	public LiikumisVahendImpl(Omanik omanik) {
		counter++;
		id = counter;
		omanikud.put(omanik.getId(), omanik);
	}

	public int getId() {
		return this.id;
	}

	public Map<Integer, Omanik> getOmanikud() {
		return omanikud;
	}

	public void setOmanikud(Map<Integer, Omanik> omanikud) {
		this.omanikud = omanikud;
	}

	@Override
	public boolean equals(Object obj){
		if(this == obj){
			return true;
		}
		if(obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		return this.getId() == ((LiikumisVahendImpl)obj).getId();
	}
	
	@Override
	public int hashCode(){
		int hash = 15;
		hash = 82 *hash + Objects.hashCode(id);
		return hash;
	}
}
