package ee.bcs.koolitus.liiklusvahend.mootorsoiduk;

import ee.bcs.koolitus.liiklusvahend.JuhiloaKategooria;
import ee.bcs.koolitus.omanik.Omanik;

public class Mootorratas extends Mootorsoiduk{

	public Mootorratas(Omanik omanik, String regNumber, JuhiloaKategooria noutudKategooria) {
		super(omanik, regNumber, noutudKategooria);
	}

}
