package ee.bcs.koolitus.liiklusvahend.mootorsoiduk;

public enum VeesoidukiTyyp {
	PAAT, LAEV, JAHT, PURJEKAS;
}
