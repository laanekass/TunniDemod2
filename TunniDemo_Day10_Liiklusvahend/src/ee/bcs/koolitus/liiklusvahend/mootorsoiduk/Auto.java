package ee.bcs.koolitus.liiklusvahend.mootorsoiduk;

import ee.bcs.koolitus.liiklusvahend.JuhiloaKategooria;
import ee.bcs.koolitus.omanik.Omanik;

public class Auto extends Mootorsoiduk {
	private AutoTyyp autoTyyp;

	public Auto(Omanik omanik, String regNumber, JuhiloaKategooria noutudKategooria) {
		super(omanik, regNumber, noutudKategooria);
	}

	public AutoTyyp getAutoTyyp() {
		return autoTyyp;
	}

	public Auto setAutoTyyp(AutoTyyp autoTyyp) {
		this.autoTyyp = autoTyyp;
		return this;
	}

	@Override
	public String toString() {
		return "Auto [autoTyyp=" + autoTyyp + ", regNumber=" + getRegNumber() + ", noutudJuhiloaKategooria()="
				+ getNoutudJuhiloaKategooria() + ", id()=" + getId() + ", omanikud()=" + getOmanikud() + "]";
	}

	
}
