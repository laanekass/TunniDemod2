package ee.bcs.koolitus.liiklusvahend.mootorsoiduk;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ee.bcs.koolitus.liiklusvahend.JuhiloaKategooria;
import ee.bcs.koolitus.liiklusvahend.LiikumisVahendImpl;
import ee.bcs.koolitus.omanik.Omanik;

public class Mootorsoiduk extends LiikumisVahendImpl {
	private static List<Mootorsoiduk> mootorsoidukid = new ArrayList<>();
	private String regNumber;
	private JuhiloaKategooria noutudJuhiloaKategooria;

	public Mootorsoiduk(Omanik omanik, String regNumber, JuhiloaKategooria noutudKategooria) {
		super(omanik);
		this.regNumber = regNumber;
		this.noutudJuhiloaKategooria = noutudKategooria;
		mootorsoidukid.add(this);
	}

	@Override
	public void soidab(String algus, String lopp) {
		// TODO Auto-generated method stub

	}

	public static Mootorsoiduk getMootorSoidukRegNrJargi(String regNumber) {
		for (Mootorsoiduk m : mootorsoidukid) {
			if (m.regNumber.equals(regNumber)) {
				return m;
			}
		}
		return null;
	}

	public static Set<Mootorsoiduk> getMootorSoidukOmanikuNimiJargi(String omanikuNimi) {
		Set<Mootorsoiduk> omanikuSoidukid = new HashSet<>();
		for (Mootorsoiduk m : mootorsoidukid) {
			for (Omanik o : m.getOmanikud().values()) {
				if (o.getOmanikuNimi().equals(omanikuNimi)) {
					omanikuSoidukid.add(m);
				}
			}
		}
		return omanikuSoidukid;
	}

	public boolean kaivitub() {
		return true;
	}

	public boolean seiskub() {
		return true;
	}

	public String getRegNumber() {
		return regNumber;
	}

	public Mootorsoiduk setRegNumber(String regNumber) {
		this.regNumber = regNumber;
		return this;
	}

	public JuhiloaKategooria getNoutudJuhiloaKategooria() {
		return noutudJuhiloaKategooria;
	}

	public Mootorsoiduk setNoutudJuhiloaKategooria(JuhiloaKategooria noutudJuhiloaKategooria) {
		this.noutudJuhiloaKategooria = noutudJuhiloaKategooria;
		return this;
	}
}
