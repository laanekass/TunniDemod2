package ee.bcs.koolitus.liiklusvahend.mootorsoiduk;

public enum AutoTyyp {
	SOIDUAUTO, VEOAUTO, TAKSO, KAUBIK;
}
