package ee.bcs.koolitus.liiklusvahend.mootorsoiduk;

import ee.bcs.koolitus.liiklusvahend.JuhiloaKategooria;
import ee.bcs.koolitus.omanik.Omanik;

public class Veesoiduk extends Mootorsoiduk{
	private VeesoidukiTyyp veesoidukiTyyp;

	public Veesoiduk(Omanik omanik, String regNumber, JuhiloaKategooria noutudKategooria) {
		super(omanik, regNumber, noutudKategooria);
	}

	public VeesoidukiTyyp getVeesoidukiTyyp() {
		return veesoidukiTyyp;
	}

	public Veesoiduk setVeesoidukiTyyp(VeesoidukiTyyp veesoidukiTyyp) {
		this.veesoidukiTyyp = veesoidukiTyyp;
		return this;
	}

	@Override
	public String toString() {
		return "Veesoiduk [veesoidukiTyyp=" + veesoidukiTyyp + ", regNumber=" + getRegNumber()
				+ ", noutudJuhiloaKategooria=" + getNoutudJuhiloaKategooria() + ", id=" + getId()
				+ ", omanikud=" + getOmanikud() + "]";
	}

	
}
