package ee.bcs.ee;

public class ConditionalStatementsDemo {

	public static void main(String[] args) {
		int a = Integer.parseInt(args[0]);
		if (a % 2 == 0) {
			System.out.println(a + " on paaris arv");
		} else {
			System.out.println(a + " ei ole paaris arv");
		}

		int b = 18;
		String kasOnPaaris = (b % 2 == 0) ? (b + " on paaris arv") : (b + " ei ole paaris arv");
		System.out.println(kasOnPaaris);
	}
}
