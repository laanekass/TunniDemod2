package ee.bcs.ee;

public class IfAndSwitchDemo {

	public static void main(String[] args) {
		String color = args[0].toLowerCase();
		if (color.equalsIgnoreCase("green")) {
			System.out.println("Driver can drive a car.");
		} else if (color.equals("yellow")) {
			System.out.println("Driver has to be ready to stop the car or to start driving..");
		} else if (color.equals("red")) {
			System.out.println("Driver has to stop car and wait for green light.");
		} else {
			System.out.println("Driver shouldn't drive in the club");
		}

		switch (color) {
		case "green":
			System.out.println("Driver can drive a car.");
			break;
		case "yellow":
			System.out.println("Driver has to be ready to stop the car or to start driving..");
			break;
		case "red":
			System.out.println("Driver has to stop car and wait for green light.");
			break;
		default:
			System.out.println("Driver shouldn't drive in the club");
		}
	}

}
