package ee.bcs.koolititus.datatypes;

public class StringDemo {

	public static void main(String[] args) {
		String text1 = "this is \nfirst \"text\"";
		String text2;
		String text3 = "";

		// System.out.println("text2 = "+ text2); //see rida annab vea, sest
		// muutuja on väärtustamata
		System.out.println("text1 = " + text1);
		text2 = "value given  \tlater";
		System.out.println("text2 = " + text2);
		System.out.println("text3 = " + text3);

		System.out.println("asi" == "asi");
		System.out.println("asi2" == ("a" + "si2"));

		String text4 = "asi";
		String text5 = "a";
		String text6 = "si";
		System.out.println(text4 == (text5 + text6));
		System.out.println(text4.equals(text5 + text6));
	}
}
