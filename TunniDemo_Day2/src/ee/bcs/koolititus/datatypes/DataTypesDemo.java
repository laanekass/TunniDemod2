package ee.bcs.koolititus.datatypes;

public class DataTypesDemo {

	public static void main(String[] args) {
		byte byte1 = 12;
		byte byte2 = -28;
		byte byte3V = -110;
		Byte byte3 = new Byte(byte3V);
		Byte byte4 = new Byte("-110");
		Byte byte5 = 127;

		byte byte6 = (byte) (byte3 + byte4);

		System.out.println(byte1 + byte2);
		System.out.println(byte1 + byte3);
		System.out.println(byte6);

		int int1 = 25;
		int int2 = -225;

		Integer int3 = 360000;
		Integer int4 = 1_000_000_000;
		Integer int5 = new Integer("-286");

		System.out.println(int1 + int2);
		System.out.println(int3 + int4);
		System.out.println(int5);

		Double double1 = (double) (new Integer("-286"));
		System.out.println(double1);

		Double double2 = new Double("-286.0");
		System.out.println(double2);
		System.out.println(int5 + double2);

		int arv1 = 8;
		boolean kasVordub = arv1 == 7;
		System.out.println("boolean on: " + kasVordub);

		int arv2 = arv1 + 8;
		int arv3 = ++arv1;

		System.out.println("arv2 = " + arv2);
		System.out.println("arv3 = " + arv3);
		System.out.println("arv1 = " + arv1);

		int arv4 = 12;
		arv4 *=8;
		System.out.println("arv4 = " + arv4);
		
		
	}

}
