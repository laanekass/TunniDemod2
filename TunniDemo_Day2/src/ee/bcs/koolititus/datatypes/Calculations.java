package ee.bcs.koolititus.datatypes;

public class Calculations {

	public static void main(String[] ar4gs) {
		int a = 1;
		int b = 1;
		int c = 3;

		System.out.println("a==b : " + (a == b));
		System.out.println("a==c : " + (a == c));
		System.out.println("-----------");

		a = c;
		System.out.println("a==b : " + (a == b));
		System.out.println("a==c : " + (a == c));
		System.out.println("-----------");

		int x1 = 10;
		int x2 = 20;
		int y1 = ++x1;
		System.out.println("x1 = " + x1 + "; y1 = " + y1);
		int y2 = x2++;
		System.out.println("x2 = " + x2 + "; y2 = " + y2);
		
		printModules();
	}

	/**
	 * See on mooduli arvutamise meetod
	 */
	private static void printModules() {
		int a = 18 % 3;
		int b = 19 % 3;
		int c = 20 % 3;
		int d = 21 % 3;
		System.out.println("a = " + a + "; b = " + b + "; c = " + c + "; d = " + d);
	}

}
