package ee.bcs.koolitus;

import java.util.List;

import ee.bcs.koolitus.autohaldus.dao.Auto;
import ee.bcs.koolitus.autohaldus.resource.AutoResource;

public class Main {

	public static void main(String[] args) {
		AutoResource autoR = new AutoResource();
		List<Auto> autod = autoR.getAllAutos();
		System.out.println(autod);
		
		System.out.println(autoR.getAutoById(1));
		
//		Auto uusAuto = new Auto().setName("Fiat").setMudel("kamiski");
//		System.out.println(autoR.addAuto(uusAuto));
		
		Auto muudaAuto = autoR.getAutoById(6);
		muudaAuto.setName("Volvo").setMudel("xR10");
		
		autoR.updateAuto(muudaAuto);
		System.out.println(autoR.getAutoById(6));
		
		autoR.deleteAuto(new Auto().setId(9));
		
	}

}
