package ee.bcs.koolitus.autohaldus.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.autohaldus.dao.Auto;
import ee.bcs.koolitus.autohaldus.resource.AutoResource;

@Path("/autod")
public class AutoController {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Auto> getAllAutos() {
		return new AutoResource().getAllAutos();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Auto getAutoById(@PathParam("id") int id) {
		return new AutoResource().getAutoById(id);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Auto addAuto(Auto uusAuto) {
		return new AutoResource().addAuto(uusAuto);
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Auto updateAuto(Auto muudetudAuto) {
		new AutoResource().updateAuto(muudetudAuto);
		return muudetudAuto;
	}

	@DELETE
	@Path("/{id}")
	public void deleteAuto(@PathParam("id") int id) {
		new AutoResource().deleteAuto(new Auto().setId(id));
	}
}
