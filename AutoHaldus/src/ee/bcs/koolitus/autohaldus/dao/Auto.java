package ee.bcs.koolitus.autohaldus.dao;

import java.time.LocalDate;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class Auto {
	private int id;
	private String name;
	private String mudel;
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate tootmisKuupaev;
	
	public int getId() {
		return id;
	}
	public Auto setId(int id) {
		this.id = id;
		return this;
	}
	public String getName() {
		return name;
	}
	public Auto setName(String name) {
		this.name = name;
		return this;
	}
	public String getMudel() {
		return mudel;
	}
	public Auto setMudel(String mudel) {
		this.mudel = mudel;
		return this;
	}	
	
	public LocalDate getTootmisKuupaev() {
		return tootmisKuupaev;
	}
	public Auto setTootmisKuupaev(LocalDate tootmisKuupaev) {
		this.tootmisKuupaev = tootmisKuupaev;
		return this;
	}
	@Override
	public String toString() {
		return "Auto [id=" + id + ", name=" + name + ", mudel=" + mudel + "]";
	}
}
