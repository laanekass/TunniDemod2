package ee.bcs.koolitus.autohaldus.resource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.autohaldus.dao.Auto;

public class AutoResource {

	public List<Auto> getAllAutos() {
		List<Auto> autod = new ArrayList<>();
		String sqlQuery = "SELECT * FROM auto";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			while (results.next()) {
				autod.add(new Auto().setId(results.getInt("auto_id")).setName(results.getString("tootja_nimi"))
						.setMudel(results.getString("mudel"))
						.setTootmisKuupaev(results.getDate("tootmiskuupaev") != null
								? results.getDate("tootmiskuupaev").toLocalDate() : null));
			}
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
		return autod;
	}

	public Auto getAutoById(int autoId) {
		Auto auto = null;
		String sqlQuery = "SELECT * FROM auto WHERE auto_id = " + autoId;
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery)) {
			while (results.next()) {
				auto = new Auto().setId(results.getInt("auto_id")).setName(results.getString("tootja_nimi"))
						.setMudel(results.getString("mudel"))
						.setTootmisKuupaev(results.getDate("tootmiskuupaev").toLocalDate());
			}
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
		return auto;
	}

	public Auto addAuto(Auto auto) {
		String sqlQuery = "INSERT INTO auto (tootja_nimi, mudel, tootmiskuupaev) VALUES('" + auto.getName() + "', '"
				+ auto.getMudel() + "', '" + auto.getTootmisKuupaev() + "')";
		try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
			statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				auto.setId(resultSet.getInt(1));
			}
			resultSet.close();
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}

		return auto;
	}

	public void updateAuto(Auto auto) {
		String sqlQuery = "UPDATE auto SET tootja_nimi = '" + auto.getName() + "', mudel = '" + auto.getMudel()
				+ "' WHERE auto_id = " + auto.getId();

		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Some error on update");
			}
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
	}

	public void deleteAuto(Auto auto) {
		String sqlQuery = "DELETE FROM auto WHERE auto_id = " + auto.getId();
		try {
			Integer code = DatabaseConnection.getConnection().createStatement().executeUpdate(sqlQuery);
			if (code != 1) {
				throw new SQLException("Some error on delete");
			}
		} catch (SQLException e) {
			System.out.println("Error on running query: " + e.getStackTrace());
		}
	}
}
