var allCars = [];
function getAllCars() {
	$
			.getJSON(
					"http://localhost:8080/AutoHaldus/rest/autod",
					function(cars) {
						allCars = cars;
						var tableContent = "";
						for (var i = 0; i < cars.length; i++) {
							tableContent = tableContent
									+ "<tr><td>"
									+ cars[i].id
									+ "</td><td>"
									+ cars[i].name
									+ "</td><td>"
									+ cars[i].mudel
									+ "</td><td>"
									+ (cars[i].tootmisKuupaev!=undefined ? cars[i].tootmisKuupaev:'')
									+ "</td><td><button type='button' class='btn btn-default' data-toggle='modal' data-target='#updateCarModal' onClick='fillChangeCarModal("
									+ cars[i].id
									+ ")'>Muuda</button><button type='button' class='btn btn-default' data-toggle='modal' data-target='#deleteCarModal' onClick='fillDeleteCarModal("
									+ cars[i].id
									+ ")'>Kustuta</button></td></tr>";
						}
						document.getElementById("carsBody").innerHTML = tableContent;
						document.getElementById("carsTotal").innerHTML = cars.length;
					});
}

getAllCars();

function addNewCar() {
	var newCarJSON = $("#newCarForm").serializeFormJSON();
	var newCar = JSON.stringify(newCarJSON);

	$.ajax({
		url : "http://localhost:8080/AutoHaldus/rest/autod",
		cache : false,
		type : 'POST',
		data : newCar,
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllCars();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		}
	});
}

var carToChangeId;
function fillChangeCarModal(carId) {
	for (var i = 0; i < allCars.length; i++) {
		if (allCars[i].id == carId) {
			document.getElementById("updateAutoId").value = carId;
			document.getElementById("updateTootjaNimi").value = allCars[i].name;
			document.getElementById("updateMudel").value = allCars[i].mudel;
			carToChangeId = carId;
		}
	}

}

function saveCarChanges() {
	var updateCarJSON = $("#updateCarForm").serializeFormJSON();
	updateCarJSON = addIdFieldInJson(updateCarJSON, carToChangeId);
	var updateCar = JSON.stringify(updateCarJSON);

	$.ajax({
		url : "http://localhost:8080/AutoHaldus/rest/autod",
		cache : false,
		type : 'PUT',
		data : updateCar,
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllCars();
			$('#updateCarModal').modal('toggle');
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		}
	});
}

function addIdFieldInJson(objectJson, fieldValue) {
	var modifiedJson = objectJson;
	modifiedJson.id = fieldValue;
	return modifiedJson;
}

var deleteCarId;
function fillDeleteCarModal(carId) {
	document.getElementById("deleteModalBody").innerHTML = "Kas oled kindel, et soovid kustuta auto, mille ID on "
			+ carId;
	deleteCarId = carId;
}

function deleteCar() {
	$.ajax({
		url : "http://localhost:8080/AutoHaldus/rest/autod/" + deleteCarId,
		cache : false,
		type : 'DELETE',
		contentType : "application/json; charset=utf-8",
		success : function() {
			getAllCars();
			$('#deleteCarModal').modal('toggle');
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("Status: " + textStatus);
			console.log("Error: " + errorThrown);
		}
	});
}

(function($) {
	$.fn.serializeFormJSON = function() {
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};
})(jQuery);