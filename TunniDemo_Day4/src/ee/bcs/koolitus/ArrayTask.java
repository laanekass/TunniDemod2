package ee.bcs.koolitus;

import java.util.Arrays;

public class ArrayTask {

	public static void main(String[] args) {
		// alternatiiv 1
		String[][] inimesed1 = new String[3][4];
		String[] mari = { "Mari", "25", "Tallinn", "5123456" };
		String[] kati = { "Kati", "28", "Tartu" };
		String[] mati = { "Mati", "30", "Pärnu", "5213465" };
		inimesed1[0] = mari;
		inimesed1[1] = kati;
		inimesed1[2] = mati;

		System.out.println("Alternatiiv 1: " + Arrays.deepToString(inimesed1));

		// alternatiiv 2
		String[][] inimesed2 = { { "Mari", "25", "Tallinn", "5123456" }, { "Kati", "28", "Tartu" },
				{ "Mati", "30", "Pärnu", "5213465" } };
		System.out.println("Alternatiiv 2: " + Arrays.deepToString(inimesed2));

		String[][] inimesed3 = new String[4][4];
		String[] riho = { "Riho", "24", "Talllinn", "5321465" };
		inimesed3[0] = inimesed2[0];
		inimesed3[1] = inimesed2[1];
		inimesed3[2] = inimesed2[2];
		inimesed3[3] = riho;
		System.out.println("Ül 2 Alternatiiv 1: " + Arrays.deepToString(inimesed3));

		String[][] inimesed4 = new String[4][4];
		System.arraycopy(inimesed2, 0, inimesed4, 0, 3);
		inimesed4[3] = riho;
		System.out.println("Ül 2 Alternatiiv 2: " + Arrays.deepToString(inimesed4));
		int pikkus = inimesed4[0].length;

		int i = 3;
		for (; i >= -55; i = i + 1000000) {
			System.out.println("i = " + i);
		}

		System.out.println("------------");
		System.out.println(inimesed4);

		for (int rida = 0; rida < inimesed4.length; rida++) {
			for (int veerg = 0; veerg < inimesed4[rida].length; veerg++) {
				System.out.print(inimesed4[rida][veerg] + ";");
			}
			System.out.println();
		}

		String[][] inimesed5 = new String[5][5];
		String[] tiit = { "Tiit", "38", "Tartu", "5689458", "programmeerija" };

		for (int inimesePos = 0; inimesePos < inimesed4.length; inimesePos++) {
			// siia tuleb ühes massiivist teise kopeerimine
			inimesed5[inimesePos] = inimesed4[inimesePos];
		}
		inimesed5[4] = tiit;

		System.out.println("Ül 3: " + Arrays.deepToString(inimesed5));

		// While näide
		int j = 5;
		while (j <= 5) {
			System.out.println("j = " + j);
			j++;
		}

		int k = 6;
		do {
			System.out.println("k = " + k);
			k++;
		} while (k <= 5);

		boolean kasOnKymme = false;

		int arv = 100;
		// kasOnKymme != true
		// kasOnKymme == false
		while (!kasOnKymme) {
			System.out.println("arv oli tsükli alguses " + arv);
			arv = arv - 10;
			if (arv == 10) {
				kasOnKymme = true;
			}
			System.out.println("arv on tsükli lõpus " + arv);
		}
		System.out.println("Lõpetas tsükli");

		// for-each
		for (String[] inimeseRida : inimesed5) {
			for (String veerg : inimeseRida) {
				System.out.print(veerg + ";");
			}
			System.out.println();
		}
	}
}
