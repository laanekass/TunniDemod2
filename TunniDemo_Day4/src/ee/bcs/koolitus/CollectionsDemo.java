package ee.bcs.koolitus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class CollectionsDemo {

	public static void main(String[] args) {
		List<String> nimed = new ArrayList<>();
		
		nimed.add("mari");
		nimed.add("kati");
		nimed.add("mati");
		
		nimed.add(1, "jüri");
		nimed.add("rain");
		
		System.out.println(nimed);
		
		List<String> nimed2 = new ArrayList<>();
		nimed2.add("tiit");
		nimed2.add("tiit");

		nimed.addAll(2, nimed2);
		System.out.println(nimed);
		System.out.println(nimed.indexOf("tiit"));
		
		String t1 = "ti";
		String t2 = "it";
		String t3 = t1 + t2;
		System.out.println(nimed.indexOf(t3));
		
		nimed.set(2, "kadri");
		System.out.println(nimed);
		
		nimed.remove("kati");
		System.out.println(nimed);
		
		List<String[]> lemmikud = new ArrayList<>();
		String[] muki = {"muki", "koer"};
		String[] jommu = {"jõmmu", "kass"};
		String[] leksus = {"Leksus", "koer"};
		String[] naki = {"naki", "koer"};
		String[] alice = {"alice", "kass"};
		
		lemmikud.add(muki);
		lemmikud.addAll(Arrays.asList(jommu, naki, leksus, alice));
		
		for(String[] lemmik: lemmikud) {
			System.out.println(Arrays.deepToString(lemmik));
		}
		
		System.out.println(lemmikud.indexOf(leksus));
		
		
		Map<String, String> inimesed = new Hashtable<>();
		inimesed.put("32165498778", "Mati Kask");
		
	}

}
