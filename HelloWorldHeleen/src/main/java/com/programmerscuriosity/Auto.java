package main.java.com.programmerscuriosity;

public class Auto {
    private int id;
    private String tootja_nimi;

    public int getId() {
        return id;
    }

    public Auto setId(int id) {
        this.id = id;
        return this;
    }

    public String getTootja_nimi() {
        return tootja_nimi;
    }

    public Auto setTootja_nimi(String tootja_nimi) {
        this.tootja_nimi = tootja_nimi;
        return this;
    }
}
