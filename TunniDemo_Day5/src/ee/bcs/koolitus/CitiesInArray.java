package ee.bcs.koolitus;

import java.util.Arrays;

public class CitiesInArray {
	public static void main(String[] args) {
		// ül 1
		String[][] countriesWithCities = new String[195][4];

		// ül 2-3
		String[] eesti = { "Tallinn", "Tallinn", "Tallinn", "Eesti" };
		String[] soome = { "Helsinki", "Helsingi", "Helsinki", "Soome" };
		String[] rootsi = { "Stockholm", "Stockholm", "Stockholm", "Rootsi" };
		String[] uk = { "London", "London", "London", "United Kingdom" };
		String[] us = { "Washington", "Washington", "Washington", "United States of America" };

		countriesWithCities[0] = eesti;
		countriesWithCities[1] = soome;
		countriesWithCities[2] = rootsi;
		countriesWithCities[3] = uk;
		countriesWithCities[4] = us;

		System.out.println(Arrays.deepToString(countriesWithCities));

		// ül 4 variatsioonid
		// Alternatiiv 1
		for (String[] country : countriesWithCities) {
			if (country[1] != null) {
				System.out.println(country[1]);
			}
		}

		// Alternatiiv 2
		for (int rida = 0; rida < countriesWithCities.length; rida++) {
			if (countriesWithCities[rida][1] != null) {
				System.out.println(countriesWithCities[rida][1]);
			}
		}

		// Alternatiiv 3
		for (int rida = 0; rida < countriesWithCities.length && countriesWithCities[rida][1] != null; rida++) {
			System.out.println(countriesWithCities[rida][1]);
		}

		// ül5 üks lahendusvariant
		for (String[] country : countriesWithCities) {
			if (country[1] != null) {
				System.out.println("Riik - " + country[3] + ": pealinn - " + country[1] + "; inglise keeles - "
						+ country[0] + ", kohalikus keeles - " + country[2]);
			}
		}

		// ül6 üks lahendus variant- Liigutan riigi nime esimeseks, erinevad
		// kohalikud keeled hakkavad olema massiivi lõpus

		// liigutan riigi esimeseks
		for (String[] country : countriesWithCities) {
			if (country[0] != null) {
				String[] temporaryCountry = country.clone();
				country[0] = temporaryCountry[3];
				country[1] = temporaryCountry[0];
				country[2] = temporaryCountry[1];
				country[3] = temporaryCountry[2];
			}
		}
		System.out.println("Peale riigi esimeseks liigutamist: " + Arrays.deepToString(countriesWithCities));
		// Vahetan soome sisu
		String[] uusSoome = { "Soome", "Helsinki", "Helsingi", "Helsinki", "Helsingfors" };
		countriesWithCities[1] = uusSoome;
		// Täienda lauset nii, et see trükiks kõiki kohalikke nimesid
		printCountriesWithLocalCityNames(countriesWithCities);

		// Ül 7
		for (String[] country : countriesWithCities) {
			country[0] = "country:" + country[0];
		}
		printCountriesWithLocalCityNames(countriesWithCities);

		// ül 9
		String[] kurrunurru = { "country:Kurrunurruvutisaare Kuningriik", "Longstocking City", "Pikksuka linn",
				"Langstrump" };
		String[][] countriesWithCities2 = new String[195][];
		countriesWithCities2[2] = kurrunurru;
		for (int riigiIndex = 0; riigiIndex < countriesWithCities.length; riigiIndex++) {
			if (riigiIndex < 2) {
				countriesWithCities2[riigiIndex] = countriesWithCities[riigiIndex];
			} else if (riigiIndex > 2) {
				countriesWithCities2[riigiIndex] = countriesWithCities[riigiIndex - 1];
			}
		}
		printCountriesWithLocalCityNames(countriesWithCities2);
	}

	private static void printCountriesWithLocalCityNames(String[][] countriesWithCities) {
		System.out.println("-------------");

		for (String[] country : countriesWithCities) {
			if (country[1] != null) {
				StringBuilder localName = new StringBuilder().append(country[3]);
				if (country.length > 4) {
					for (int i = 4; i < country.length; i++) {
						localName.append(", " + country[i]);
					}
				}
				// ül 8 jaoks- et country: sõna ei trükitaks
				String riigiNimi = (country[0].split(":").length > 1) ? country[0].split(":")[1] : country[0];

				System.out.println("Riik - " + riigiNimi + ": pealinn - " + country[2] + "; inglise keeles - "
						+ country[1] + ", kohalikus keeles - " + localName);
			}
		}
	}
}
