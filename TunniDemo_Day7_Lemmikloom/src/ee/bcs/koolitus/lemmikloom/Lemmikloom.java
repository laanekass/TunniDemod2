package ee.bcs.koolitus.lemmikloom;

import java.util.ArrayList;
import java.util.List;

public class Lemmikloom {
	static int counter = 0;
	public static final List<Lemmikloom> lemmikloomad = new ArrayList<>();
	private int id;
	private String nimi;
	private String synnipaev;
	private LemmikloomaLiik liik;

	public Lemmikloom(String nimi) {
		counter = counter + 10;
		id = counter;
		this.nimi = nimi;
		lemmikloomad.add(this);
	}

	public Lemmikloom(String nimi, String synnipaev, LemmikloomaLiik liik) {
		this(nimi);
		this.synnipaev = synnipaev;
		this.liik = liik;
	}

	public String getNimi() {
		return nimi;
	}

	public Lemmikloom setNimi(String nimi) {
		this.nimi = nimi;
		return this;
	}

	public String getSynnipaev() {
		return synnipaev;
	}

	public Lemmikloom setSynnipaev(String synnipaev) {
		this.synnipaev = synnipaev;
		return this;
	}

	public LemmikloomaLiik getLiik() {
		return liik;
	}

	public Lemmikloom setLiik(LemmikloomaLiik liik) {
		this.liik = liik;
		return this;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Lemmikloom [id=" + id + ", nimi=" + nimi + ", synnipaev=" + synnipaev + ", liik=" + liik + "]";
	}
}
