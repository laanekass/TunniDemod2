package ee.bcs.koolitus.lemmikloom;

public class Main {
	public static void main(String[] args) {
		Lemmikloom alice = new Lemmikloom("Alice");
		alice.setLiik(LemmikloomaLiik.KASS).setSynnipaev("15.09.1992");
		//System.out.println(alice.nimi);
		//System.out.println(alice.synnipaev);
		Lemmikloom leksus = new Lemmikloom("Leksus", "27.06.2016", LemmikloomaLiik.KOER);
		
		System.out.println(alice.getId() + ", " + alice.getNimi());
		System.out.println(leksus);
		
		System.out.println("Lemmikud: " + Lemmikloom.lemmikloomad);

	}
}
