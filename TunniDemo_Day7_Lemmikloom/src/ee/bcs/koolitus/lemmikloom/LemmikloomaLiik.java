package ee.bcs.koolitus.lemmikloom;

public enum LemmikloomaLiik {
	KOER, KASS, KILPKONN, REBANE, JÄNES, ROTT, HOBUNE;
}
