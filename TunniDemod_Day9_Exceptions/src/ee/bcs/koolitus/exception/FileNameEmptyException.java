package ee.bcs.koolitus.exception;

public class FileNameEmptyException  extends Exception{
	
	public FileNameEmptyException(){
		super();
	}
	
	public FileNameEmptyException(String message) {
		super(message);
	}

}
