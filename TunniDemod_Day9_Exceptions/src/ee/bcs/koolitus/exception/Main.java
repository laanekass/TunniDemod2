package ee.bcs.koolitus.exception;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		// FileReader fileReader = null;
		// try {
		// fileReader = new FileReader("testFile.txt");
		// } catch (FileNotFoundException e) {
		// System.out.println("Faili ei leitud: ");
		// e.printStackTrace();
		// } finally {
		// try {
		// fileReader.close();
		// } catch (IOException | NullPointerException e) {
		// System.out.println("Readerit ei saanud sulgeda: " + e.getMessage());
		// }
		// }
		//
		// try (FileReader fileReader1 = new FileReader("testFile.txt")){
		// //loen failist ja teen midagi
		// } catch (FileNotFoundException e) {
		// System.out.println("Faili ei leitud: ");
		// e.printStackTrace();
		// } catch (IOException e1) {
		// System.out.println("Readerit ei saanud sulgeda: " + e1.getMessage());
		// }

		try {
			writeFairyTale("");
		} catch (FileNameEmptyException e) {
			System.out.println(e.getMessage());
		}
	}

	static private void writeFairyTale(String fileName) throws IOException, FileNameEmptyException {
		if(fileName == null || fileName.equals("")){
			throw new FileNameEmptyException("Loo kirjutamiseks on faili nime vaja");
		}
		FileWriter fw = new FileWriter(fileName);
		
	}

}
