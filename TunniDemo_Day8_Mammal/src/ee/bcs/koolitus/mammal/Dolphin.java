package ee.bcs.koolitus.mammal;

public class Dolphin extends Mammal{

	@Override
	public void moveAhead() {
		swimAhead();
		swimToSurface();
		breathe();
		dive();
		swimAhead();
	}

	private void swimAhead(){
		System.out.println(this.speciesName + ", dolphin, swims ahead");
	}
	
	private void dive() {
		System.out.println(this.speciesName + ", dolphin, dives");
	}
	
	private void breathe() {
		System.out.println(this.speciesName + ", dolphin, breathes");
	}
	
	private void swimToSurface() {
		System.out.println(this.speciesName + ", dolphin, swims to the surface");
	}
	
}
