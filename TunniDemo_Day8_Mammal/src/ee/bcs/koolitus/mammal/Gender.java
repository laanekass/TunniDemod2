package ee.bcs.koolitus.mammal;

public enum Gender {
	MALE, FEMALE;
}
