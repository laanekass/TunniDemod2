package ee.bcs.koolitus.mammal;

public class Main {

	public static void main(String[] args) {
		Human human = new Human();
		human.defineSpeciesName("Mart");
		human.moveAhead();

		Mammal dolphin = new Dolphin();
		dolphin.defineSpeciesName("Lucky");
		dolphin.moveAhead();

		String species = args[0];
		String name = args[1];
		System.out.println(species + ", " + name);

		Mammal mammal1 = createMammalBySpecies(species);
		if (mammal1 != null) {
			mammal1.defineSpeciesName(name);
			mammal1.moveAhead();
		}
	}

	private static Mammal createMammalBySpecies(String species) {
		Mammal mammal = null;
		switch (species) {
		case "dolphin":
			mammal = new Dolphin();
			break;
		case "human":
			mammal = new Human();
			break;
		default:
			System.out.println("Unknown species");
		}
		return mammal;
	}

}
