package ee.bcs.koolitus.mammal;

public class Human extends Mammal {
	public boolean isStanding = false;

	@Override
	public void moveAhead() {
		if(!isStanding){
			standUp();
		}
		makeStepWithLeg(Leg.LEFT);
		makeStepWithLeg(Leg.RIGHT);
	}

	private void standUp() {
		System.out.println(this.speciesName + ", human, stands up");
	}

	private void makeStepWithLeg(Leg leg) {
		System.out.println(this.speciesName + ", human, made a step ahead with " + leg + " leg.");
	}
}
