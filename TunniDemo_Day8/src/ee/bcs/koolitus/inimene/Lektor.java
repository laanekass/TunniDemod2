package ee.bcs.koolitus.inimene;

public class Lektor extends Inimene {
	private boolean istekohtKlassiEes = false;
	
	public Lektor(){
		super("LektorX");
		System.out.println("Lektor on valmis tehtud");
	}
	
	public Lektor(String nimi) {
		super(nimi);
	}

	public String raagib(String lause) {
		return "Lektor ütleb: \"" + lause + "\"";
	}

	public boolean hasIstekohtKlassiEes() {
		return istekohtKlassiEes;
	}

	public Lektor setIstekohtKlassiEes(boolean istekohtKlassiEes) {
		this.istekohtKlassiEes = istekohtKlassiEes;
		return this;
	}

	@Override
	public String toString() {
		return "Lektor [nimi=" + this.getNimi() + ", vanus=" + this.getVanus() + ", aadress=" + this.getAadress()
				+ ", istekohtKlassiEes=" + istekohtKlassiEes + "]";
	}

}