package ee.bcs.koolitus.inimene;

public class Main {

	public static void main(String[] args) {
		Inimene inimene = new Inimene("Mati");
		inimene.setNimi("Mati2");
		inimene.setVanus(25);
		System.out.println(inimene);
		inimene.kysibKysimuse("Miks päike tõuseb idast?");

		Inimene lektor1 = new Lektor();
		System.out.println("Lektor 1: " + lektor1.getNimi());
		lektor1.setNimi("Henn");
		((Lektor) lektor1).setIstekohtKlassiEes(true);
		System.out.println("Lektor 1: " + ((Lektor) lektor1).getNimi());
		System.out.println("Lektor 1: " + lektor1.getNimi());
		System.out.println("Lektor 1: " + ((Lektor) lektor1).hasIstekohtKlassiEes());
		lektor1.kysibKysimuse("Mis asi on .net?");

		Lektor lektor2 = new Lektor("Heleen");
		System.out.println("Lektor 2: " + lektor2);
		lektor2.kysibKysimuse("Mis asi on pärimine?");
		System.out.println(lektor2.raagib("Laiendava classi nimele järgneb extends VanemaKlassiNimi"));
		System.out.println(lektor2);

		Opilane opilane = new Opilane("Kati");
		opilane.setIsteKohtKlassis(true);
		
		System.out.println(opilane);
		opilane.kysibKysimuse("Mille jaoks on konstruktor");
		opilane.kuulab();
	}

}
