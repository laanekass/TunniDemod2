package ee.bcs.koolitus.inimene;

import java.util.ArrayList;
import java.util.List;

public class Opilane extends Inimene{
	private boolean isteKohtKlassis = false;
	private List<String> markmed = new ArrayList<>();
	
	public Opilane(String nimi) {
		super(nimi);
	}

	public void kuulab() {
		System.out.println("Õpilane " + this.getNimi() + " kuulab.");
	}

	@Override
	public void kysibKysimuse(String kysimus) {
		System.out.println("Õpilane ootab küsimiseks sobiva hetke");
		System.out.println("Õpilane küsib: " + kysimus);
	}

	public boolean hasIsteKohtKlassis() {
		return isteKohtKlassis;
	}

	public void setIsteKohtKlassis(boolean isteKohtKlassis) {
		this.isteKohtKlassis = isteKohtKlassis;
	}

	public List<String> getMarkmed() {
		return markmed;
	}

	public void setMarkmed(List<String> markmed) {
		this.markmed = markmed;
	}

	@Override
	public String toString() {
		return "Opilane [nimi=" + this.getNimi() + ", vanus=" + this.getVanus() + ", aadress=" + this.getAadress()
				+ ", isteKohtKlassis=" + isteKohtKlassis + ", markmed=" + markmed + "]";
	}

}
