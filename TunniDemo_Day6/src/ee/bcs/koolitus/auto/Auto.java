package ee.bcs.koolitus.auto;

import java.util.ArrayList;
import java.util.List;

public class Auto {
	int usteArv;
	int kohtadeArv;
	Mootor mootor;
	boolean mootorKaib = false;
	int kaikHetkel = 0;
	int id;
	static int counter = 0;
	public static final List<Auto> autod = new ArrayList<>();

	public Auto() {
		counter++;
		id = counter;
		autod.add(this);
	}

	public Auto(Mootor mootor) {
		this.mootor = mootor;
		counter++;
		id = counter;
		autod.add(this);
	}

	public void kaivitaMootor() {
		if (mootor.getKytuseTyyp().equals(KytuseTyyp.DIISEL)) {
			System.out.println("Eelsüüde alustatud");
			System.out.println("Eelsüüde lõpetatud");
		}
		this.mootorKaib = true;
		System.out.println("Mootor käivitatud");
	}

	public void soida() {
		if (!mootorKaib) {
			System.out.println("Enne sõitma hakkamist peab mootor käima");
			kaivitaMootor();
		}
		if (mootor.getKaigukast().equals(KaiguKast.MANUAAL)) {
			System.out.println("sidur alla, esimene käik sisse, sidur ja pidur vabaks ja vajutada gaasi");
			System.out.println("sidur alla, teine käik sisse, sidur vabaks ja vajutada gaasi");
			System.out.println("sidur alla, kolmas käik sisse, sidur vabaks ja vajutada gaasi");
			System.out.println("sidur alla, neljas käik sisse, sidur vabaks ja vajutada gaasi");
		} else {
			System.out.println("Pidur alla, drive režiim sisse, pidur vabaks ja vajutada gaasi");
		}
	}

	public void vahetaKaiku(int maxKaik) {
		if (kaikHetkel < maxKaik) {
			kaikHetkel++;
			System.out.println(kaikHetkel + ". käik on sees");
			vahetaKaiku(maxKaik);
		}
	}

	public int getUsteArv() {
		return usteArv;
	}

	public Auto setUsteArv(int usteArv) {
		this.usteArv = usteArv;
		return this;
	}

	public int getKohtadeArv() {
		return kohtadeArv;
	}

	public Auto setKohtadeArv(int kohtadeArv) {
		this.kohtadeArv = kohtadeArv;
		return this;
	}

	public Mootor getMootor() {
		return mootor;
	}

	public Auto setMootor(Mootor mootor) {
		this.mootor = mootor;
		return this;
	}

	public boolean isMootorKaib() {
		return mootorKaib;
	}

	public int getId() {
		return id;
	}
	
	public List<Auto> getAutod() {
		return autod;
	}

	@Override
	public String toString() {
		return "Auto [id= "+ id+", usteArv=" + usteArv + ", kohtadeArv=" + kohtadeArv + ", mootor=" + mootor + "]";
	}

}
