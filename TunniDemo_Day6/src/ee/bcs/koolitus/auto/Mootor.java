package ee.bcs.koolitus.auto;

public class Mootor {
	int voimsus;
	KytuseTyyp kytuseTyyp;
	KaiguKast kaigukast;

	public int getVoimsus() {
		return this.voimsus;
	}

	public Mootor setVoimsus(int voimsus) {
		this.voimsus = voimsus;
		return this;
	}

	public KytuseTyyp getKytuseTyyp() {
		return this.kytuseTyyp;
	}

	public Mootor setKytuseTyyp(KytuseTyyp kytuseTyyp) {
		this.kytuseTyyp = kytuseTyyp;
		return this;
	}

	public KaiguKast getKaigukast() {
		return kaigukast;
	}

	public Mootor setKaigukast(KaiguKast kaigukast) {
		this.kaigukast = kaigukast;
		return this;
	}

	@Override
	public String toString() {
		return "Mootor[võimsus=" + this.voimsus + ", kütuseTüüp=" + this.kytuseTyyp + ", käigukast=" + this.kaigukast + "]";
	}
}
