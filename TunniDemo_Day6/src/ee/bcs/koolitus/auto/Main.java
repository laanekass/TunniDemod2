package ee.bcs.koolitus.auto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
	
	public static void koikAutodSoidavad(Auto... autod){
		for(Auto auto:autod) {
			System.out.println("--------------------");
			auto.soida();
			System.out.println("--------------------");
		}
	}
	public static void main(String[] args) {
		
		Mootor m1 = new Mootor();
		m1.setVoimsus(250)
			.setKytuseTyyp(KytuseTyyp.BENSIIN)
			.setKaigukast(KaiguKast.MANUAAL);
		Auto auto1 = new Auto(m1);
		System.out.println(m1);
		System.out.println(auto1);		//System.out.println("Esimene auto hakkab sõitma");
		auto1.setKohtadeArv(2)
			.setUsteArv(2)
			.setMootor(m1);			//.soida();		
		Auto auto2 = new Auto();
		auto2.setKohtadeArv(5);
		auto2.setUsteArv(5);
		Mootor m2 = new Mootor();
		m2.setVoimsus(87);
		m2.setKytuseTyyp(KytuseTyyp.DIISEL);
		m2.setKaigukast(KaiguKast.AUTOMAAT);
		auto2.setMootor(m2);
		System.out.println(auto2);		//System.out.println("Teine auto hakkab sõitma");		//auto2.kaivitaMootor();		//auto2.soida();
		koikAutodSoidavad(auto1, auto2);
		
		auto2.vahetaKaiku(5);
		
		System.out.println(new Auto());
		System.out.println(new Auto());
		System.out.println(new Auto());

//		Auto[] autod = { auto1, auto2 };
//		System.out.println("Massivist:" + Arrays.deepToString(autod));
//
//		List<Auto> autod2 = new ArrayList<>();
//		autod2.add(auto1);
//		autod2.add(auto2);
//		System.out.println("Listist: " + autod2);
		
		System.out.println(Auto.autod);
		System.out.println(auto2.getAutod());
	}

}
